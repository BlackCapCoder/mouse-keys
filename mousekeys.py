from pynput import keyboard
from pymouse import PyMouse
import threading
import time

move_step_x  = 3200/1.5
move_step_y  = 1800/1.5
sleep_time   = 1/60

btn_N  = 'k'
btn_E  = 'l'
btn_S  = 'j'
btn_W  = 'h'
btn_NE = 'u'
btn_NW = 'y'
btn_SE = 'n'
btn_SW = 'b'
btn_click = '.'

# --------------------

up_pressed    = False
down_pressed  = False
left_pressed  = False
right_pressed = False

up_time = 0
down_time = 0
left_time = 0
right_time = 0

m = PyMouse()

def myThread ():
    #global up_pressed, down_pressed, left_pressed, right_pressed, m
    global up_time, down_time, left_time, right_time

    while True:
        if (up_pressed or down_pressed or left_pressed or right_pressed):
            (x,y) = m.position()
            t     = time.clock()

            if up_pressed:    y -= move_step_y * (t - up_time) * 10
            if down_pressed:  y += move_step_y * (t - down_time) * 10
            if left_pressed:  x -= move_step_x * (t - left_time) * 10
            if right_pressed: x += move_step_x * (t - right_time) * 10

            up_time    = t
            down_time  = t
            left_time  = t
            right_time = t

            m.move(int(x), int(y))
        time.sleep(sleep_time)

t = threading.Thread(target=myThread)
t.daemon = True
t.start()


def on_press(key):
    try: k = key.char
    except: return

    global up_pressed, down_pressed, left_pressed, right_pressed
    global up_time, down_time, left_time, right_time

    if k == btn_W or k == btn_NW or k == btn_SW:
        if not left_pressed: left_time = time.clock()
        left_pressed = True
    if k == btn_S or k == btn_SE or k == btn_SW:
        if not down_pressed: down_time    = time.clock()
        down_pressed = True
    if k == btn_N or k == btn_NE or k == btn_NW:
        if not up_pressed: up_time    = time.clock()
        up_pressed = True
    if k == btn_E or k == btn_NE or k == btn_SE:
        if not right_pressed: right_time    = time.clock()
        right_pressed = True

    if k == btn_click:
        (x,y) = m.position()
        m.click (x,y)

def on_release(key):
    try: k = key.char
    except: return

    global up_pressed, down_pressed, left_pressed, right_pressed

    if k == btn_W or k == btn_NW or k == btn_SW: left_pressed  = False
    if k == btn_S or k == btn_SE or k == btn_SW: down_pressed  = False
    if k == btn_N or k == btn_NE or k == btn_NW: up_pressed    = False
    if k == btn_E or k == btn_NE or k == btn_SE: right_pressed = False


lis = keyboard.Listener(on_press=on_press, on_release=on_release)
lis.start()
lis.join()
