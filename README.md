# Mouse keys

This is a python script that let you move the mouse cursor around using the keyboard. The default keybindings are:
```
yku
h.l
bjn
```

where the `.` key performs a left click, and everything else moves the mouse cursor in the direction away from the `.`.



The reason I made this is because most 3D first person games naively assume that everybody wants to control the game using either a mouse or a controller, and so looking around is not rebindable to the keyboard. Even fewer support eight directional movement.
